# SPDT RF Switches

## Features

- Broad frequency ranges: 9 kHz to 8 GHz (PE42553) and 0.1 GHz to 6 GHz (PE42420).
- High isolation: 45 dB at 3 GHz (PE42553) and 62 dB at 3 GHz (PE42420).
- Good return loss characteristics.

## Documentation

Full documentation for the switches is available at
[RF Blocks](https://rfblocks.org/boards/SPDT-RF-Switches.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
